package TestFiles;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

public class Flipkart {
    WebDriver driver;
    JavascriptExecutor jse;

    public void invokeBrowser() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "/home/dell/IdeaProjects/Selenium_Project/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get("https://www.flipkart.com/");
    }

    public void Login() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div[2]/div/div/button")).click();

    }

    public void SearchItem() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[2]/form/div/div/input")).sendKeys("iphone 12");
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[2]/form/div/button")).click();
        Thread.sleep(2000);
    }

    public void ChoosingProduct() throws InterruptedException {
        driver.findElement(By.className("_4rR01T")).click();
        ArrayList<String> AllTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(AllTabs.get(1));
        jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0,500)");
        Thread.sleep(2000);
    }

    public void AddToCart() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[1]/div[2]/div/ul/li[1]/button")).click();
        Thread.sleep(2000);
    }

    public void SeeingGiftCardsByHovering() throws InterruptedException {
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[3]/div/div"))).build().perform();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[3]/div/div/div[2]/div[2]/div/ul/li[6]/a")).click();
    }


    public static void main(String[] args) throws Exception {
        Flipkart obj = new Flipkart();
        obj.invokeBrowser();
        obj.Login();
        obj.SearchItem();
        obj.ChoosingProduct();
        obj.AddToCart();
        obj.SeeingGiftCardsByHovering();

    }
}
